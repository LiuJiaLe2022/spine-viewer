﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Spine.Unity;
using System.Linq;
using System.Text;
using System.IO;
using System.Diagnostics;

public class MainUI : MonoBehaviour
{
    public GameObject toggleTemplate;
    SkeletonAnimation skeletonAnimation;

    private void Awake()
    {
        SpineCreateControl.Instance.onSkelAnimCreate = SetSpineSlots;
    }

    public void ExportSlotList()
    {
        var slots = skeletonAnimation.skeleton.Slots;
        List<string> slotStrList = slots.Select((x) => x.Data.Name).ToList();
        StringBuilder sb = new StringBuilder();
        slotStrList.ForEach((x) => sb.AppendLine(x));
        File.WriteAllText("slots.txt", sb.ToString());
        Process.Start(System.Environment.CurrentDirectory);
    }

    private void SetSpineSlots(SkeletonAnimation skelAnim)
    {
        skeletonAnimation = skelAnim;
        var slots = skeletonAnimation.skeleton.Slots;
        if (slots.Count > 0)
        {
            foreach (var item in slots)
            {
                GameObject slotObj = Instantiate<GameObject>(toggleTemplate,toggleTemplate.transform.parent);
                slotObj.SetActive(true);
                string slotName = item.Data.Name;
                slotObj.GetComponentInChildren<Text>().text = slotName;
                slotObj.GetComponent<Toggle>().onValueChanged.AddListener((value) =>
                {
                    skeletonAnimation.skeleton.FindSlot(slotName).A = value ? 1 : 0;
                });
            }
        }
    }
}
