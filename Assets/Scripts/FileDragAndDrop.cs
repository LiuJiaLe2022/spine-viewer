﻿using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using B83.Win32;
using System.Text;
using System;
using System.IO;

public class FileDragAndDrop : MonoBehaviour
{
    UnityDragAndDropHook hook;
    bool isAdministrator = false;
    private void Awake()
    {
        // UnityEngine.Screen.SetResolution(1280,720,false);
    }
    // important to keep the instance alive while the hook is active.
    void Start()
    {
        hook = new UnityDragAndDropHook();
            hook.InstallHook();
            hook.OnDroppedFiles += OnFiles;
    }
 
    void OnDisable()
    {
        if (null!=hook)
        {
        hook.UninstallHook();
        }
    }
    private void OnDestroy()
    {
        if (null != hook)
        {
            hook.UninstallHook();
            GC.Collect();
        }
    }
    void OnFiles(List<string> aFiles, POINT aPos)
    {
        string filepath = aFiles[0];
        if(filepath.EndsWith(".json") || filepath.EndsWith(".skel"))
        {
            SpineCreateControl.Instance.LoadTheSpine(filepath);
        }
    }
}
