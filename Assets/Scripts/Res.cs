﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Res {
    public static Sprite LoadSprite (string path) {
        Texture2D t2d = LoadTexture (path);
        if (t2d == null) return null;
        Sprite sp = Sprite.Create (t2d, new Rect (0, 0, t2d.width, t2d.height), Vector2.zero);
        return sp;
    }

    public static GameObject LoadObj (string path) {
        return GameObject.Instantiate<GameObject> (LoadObject (path) as GameObject);
    }

    public static void LoadGameObjectAsync (string path, Action<GameObject> callback) {
        LoadObjectAsync (path, (a) => {
            if (callback != null)
                callback (a as GameObject);
        });
    }

    public static AudioClip LoadAudioClip (string path) {
        return Resources.Load<AudioClip> (path);
    }

    public static void LoadAudioClipAsync (string path, Action<AudioClip> callback) {
        LoadObjectAsync (path, (a) => {
            if (callback != null) callback (a as AudioClip);
        });
    }

    public static Texture2D LoadTexture (string path) {
        return Resources.Load<Texture2D> (path);
    }

    public static UnityEngine.Object LoadObject (string path) {
        return Resources.Load<UnityEngine.Object> (path);
    }

    public static void LoadObjectAsync (string path, Action<UnityEngine.Object> callback) {
        ResourceRequest quest = Resources.LoadAsync<UnityEngine.Object> (path);
        quest.completed += (ay) => {
            if (callback != null)
                callback (quest.asset);
        };
    }

    public static IEnumerator LoadExternalTextAssets(string assetPath,Action<UnityEngine.TextAsset> callback)
    {
        string p = "file:///" + assetPath;
        WWW www = new WWW(p);
        yield return www;
        if (string.IsNullOrEmpty(www.error))
        {
            if (callback != null)
            {
                callback(new TextAsset(www.text));
            }
        }
    }

    public static IEnumerator LoadExternalTexture(string imgPath,Action<Texture2D> callback)
    {
        string p = "file:///" + imgPath;
        WWW www = new WWW(p);
        yield return www;
        if (string.IsNullOrEmpty(www.error))
        {
            if (callback != null)
            {
                callback(www.texture);
            }
        }
    }

    public static IEnumerator LoadAssetBundle(string bundlePath, Action<AssetBundle> callback)
    {
        string mUrl = "file:///" + bundlePath;
        //Debug.Log(mUrl);
        WWW www = new WWW(mUrl);
        yield return www;
        if (!string.IsNullOrEmpty(www.error))
            Debug.LogError(www.error);
        else
        {
            if (callback != null) callback(www.assetBundle);
        }
        www.Dispose();
    }

    public static IEnumerator LoadAsset(string bundlePath, string resName,Action<UnityEngine.Object> callback)
    {
        string mUrl = "file:///" + bundlePath;
        //Debug.Log(mUrl);
        WWW www = new WWW(mUrl);
        yield return www;
        if (!string.IsNullOrEmpty(www.error))
            Debug.LogError(www.error);
        else
        {
            UnityEngine.Object obj = www.assetBundle.LoadAsset(resName);
            if (callback != null) callback(obj);
            www.assetBundle.Unload(false);
        }
        www.Dispose();
    }

}